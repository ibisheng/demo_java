package com.example.init;

import com.example.config.Config;
import com.example.service.FileMgr;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class Init implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments applicationArguments) {
        Config.init();
        FileMgr.loadFiles();
    }
}